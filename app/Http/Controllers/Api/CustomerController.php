<?php

namespace App\Http\Controllers\Api;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Customer;

//use Illuminate\Database\Eloquent\Collection;


use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Model;
use App\Http\Resources\CustomerCollection;
use App\Http\Resources\CustomerResource;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return new CustomerCollection(Customer::latest()->get());
       // return Customer::all();
    }
    public function search($field,$query){
        
        $customer=Customer::where($field,'LIKE','%$query%')->latest()->paginate(10);
        return new CustomerCollection($customer);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $this->validate($request,[
            'name'=>'required|min:3|max:30',
            'phone'=>'required',


       ]);
       $customer= new Customer;

       $customer->name=$request->name;
       $customer->phone=$request->phone;
       $customer->email=$request->email;
       $customer->address=$request->address;
       $customer->postcode=$request->postcode;

       $customer->save();

        return new CustomerResource($customer);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        return new CustomerResource(Customer::findOrFail($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       /* $this->validate($request,[
            'name'=>'required|min:3|max:30',
            'phone'=>'required',


       ]);*/
       $customer= Customer::find($id);

       $customer->name=$request->name;
       $customer->phone=$request->phone;
       $customer->email=$request->email;
       $customer->address=$request->address;
       $customer->postcode=$request->postcode;

       $customer->update();

        return new CustomerResource($customer);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $customer=Customer::findOrFail($id);

        $customer->delete();
        return 'Deleted ';
    }
}
